# CLASSIFIERS IN PYTHON

## Python self-made classifiers for challenges: https://gonito.net/challenge/paranormal-or-skeptic and https://git.wmi.amu.edu.pl/dawjur/guess-reddit-date-sumo.git
### Include:
*  Naive-Bayes classifier (naive_bayes.py)
*  Linear-regression classifier with gradient-descent (linearreg.py)