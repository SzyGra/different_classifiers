#!/usr/bin/python3
# -*- coding: utf-8 -*-
from collections import defaultdict
import math
import pickle
import re
import os
import time
import csv
import random

clear = lambda: os.system('cls')
def load_vocabulary(path):
    vocabulary={}
    with open(path,'r',newline='\n',encoding='utf-8') as file:
        file_reader = csv.reader(file, delimiter=';')
        tab=[]
        for line in file_reader:
             tab.append(line)
        for el in range(0,len(tab)):
            vocabulary[tab[el][0]]=tab[el][1]
    return vocabulary
def save_vocabulary(data,path):
    with open(path,'w+',encoding='utf-8') as file:
         for class_ in data.keys(): # sceptic paranormal
            for token,number in data[class_].items():
                file.write(f'{token};{number}\n')
def tokenize_string(string):
    words=[]
    string=string.replace('\\n',' ')
    text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', string)
    string=''
    for i in text:
        string+=i
    words=re.split(';+|,+|\*+|\n+| +|\_+|\%+|\t+|\[+|\]+|\.+|\(+|\)+|\++|\\+|\/+|[0-9]+|\#+|\'+|\"+|\-+|\=+|\&+|\:+|\?+|\!+|\^+|\·+',string)
    regex_clean=re.compile(r'http|^[a-zA-Z]$|org')
    filtered=[i for i in words if not regex_clean.match(i)]
    filtered[:] = (value.lower() for value in filtered if len(value)!=0)
    return filtered
def createvoc(in_path):
    vocabulary = {'vocabulary':defaultdict(int)}# dzienik zawierajacy slownik w ktorym s slowa i ile razy wystepuja
    index=0
    with open(in_path,encoding='utf-8') as infile:
        for line in infile:
            index+=1
            tokens = tokenize_string(line)
            print(index)
            for token in tokens:
                if token not in vocabulary:
                    vocabulary['vocabulary'][token]+=1
    print(vocabulary)
    return vocabulary
def train_model(vocabulary,in_train,exp_train):
    learning_rate=0.00000000001
    word_to_index={}
    index_to_word={}
    words_voc={}
    with open(in_train,encoding='utf-8') as infile, open(exp_train,encoding='utf-8')  as  expectedfile:
        for line, exp in zip(infile, expectedfile):
            words_voc[line]=int(exp)
    weights={}
    wagi={}
    #print(vocabulary)
    iteration=0
    loss_sum=0.0
    error_big=10
    for ix in vocabulary.keys():
        weights[ix]=random.uniform(-0.01,0.01)
    weight_0=random.uniform(-0.01,0.01)
    while iteration<10000:
        #print(weights)
        d,y=random.choice(list(words_voc.items()))
        #print(d,y)
        y_hat=weight_0
        tokens=tokenize_string(d)
        for token in tokens:
            if token in vocabulary.keys():
                #print(word_to_index[token])
                y_hat+=weights[token]*tokens.count(token)
        delta=(y_hat-y)*learning_rate
        weight_0=weight_0-delta
        #print(y_hat)
        for w in tokens:
            if w in word_to_index.keys():
                weights[w]-=(tokens.count(w))*delta
        loss=(y_hat-y)*(y_hat-y)
        #print(loss)
        loss_sum+=loss
        print(iteration)
        if iteration%1000==0 and iteration!=0:
            print(f'error: {loss_sum/1000}')
            if error_big>(loss_sum/1000):
                error_big=(loss_sum/1000)
                wagi=weights
            loss_sum=0.0
        iteration+=1
    print(f'error: {loss_sum/1000}')
    #print(wagi)
    return wagi,vocabulary,weight_0
def predict(in_tsv,path_out,weights,word_to_index,weight_0):
    print(f'Wagi: {weights}')
    with open(in_tsv,encoding='utf-8') as infile,open(path_out,'w+',encoding='utf-8') as o:
        for line in infile:
            y_hat=weight_0
            tokens=tokenize_string(line)
            for token in tokens:
                if token in word_to_index.keys():
                #print(word_to_index[token])
                    y_hat+=weights[token]*(tokens.count(token))
            print(y_hat)
            if y_hat>0.5:
                o.write('1\n')
            else:
                o.write('0\n')
    o.close()
    infile.close()
def test(out,exp):
    amount=0
    all_app=0
    table_exp=[]
    table_o=[]
    with open(out,encoding='utf-8') as o,open(exp,encoding='utf-8') as ex:
        for l_e in ex:
          print(l_e.strip())
          table_exp.append(int(l_e.strip()))
        for lo in o:
            print(lo)
            table_o.append(int(lo.strip()))
    for i in range(0,len(table_exp)):
        if table_o[i]==table_exp[i]:
            amount+=1
    o.close()
    ex.close()
    print(round((amount/len(table_exp)),8))
def main(): 
    #create vocabulary with all words 
    words =createvoc("D:/IS2020/train/in.tsv")
    #save vocabulary to file
    save_vocabulary(words,"D:/IS2020/vocabulary.tsv")
    #load vocabulary
    vocabulary=load_vocabulary("D:/IS2020/vocabulary.tsv")

    #train model - return weights for every word
    weights,word_to_index,waga_zero=train_model(vocabulary,"D:/IS2020/train/in.tsv","D:/IS2020/train/expected.tsv")
    print('Predict')
    #prediction
    predict("D:/IS2020/test-A/in.tsv","D:/IS2020/test-A/out.tsv",weights,word_to_index,waga_zero)
    #Ucomment to test accuracy
    #test("D:/IS2020/dev-0/out.tsv","D:/IS2020/dev-0/expected.tsv")

main()