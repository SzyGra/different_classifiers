#!/usr/bin/python3
# -*- coding: utf-8 -*-
from collections import defaultdict
import math
import pickle
import re
import os
import time

clear = lambda: os.system('cls')

def calc_class_logprob(expected_path):
    paranolmal_classcount=0
    sceptic_classcount=0
    with open(expected_path,encoding='utf-8') as f:
        for line in f:
            if 'P' in line:
                paranolmal_classcount +=1
            else:
                sceptic_classcount +=1

    paranol_prob = paranolmal_classcount / (paranolmal_classcount + sceptic_classcount)
    sceptic_prob = sceptic_classcount / (paranolmal_classcount + sceptic_classcount)

    print(math.log(paranol_prob),math.log(sceptic_prob))

    return math.log(paranol_prob), math.log(sceptic_prob)
def save_vocabulary(data,path):
    with open(path,'w+',encoding='utf-8') as file:
        for word in data:
            file.write(f'{word}\n')

def tokenize_string(string):
    words=[]
    string=string.replace('\\n',' ')
    text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', string)
    string=''
    for i in text:
        string+=i
    words=re.split(';+|,+|\*+|\n+| +|\_+|\%+|\t+|\[+|\]+|\.+|\(+|\)+|\++|\\+|\/+|[0-9]+|\#+|\'+|\"+|\-+|\=+|\&+|\:+|\?+|\!+|\^+|\·+',string)
    regex_clean=re.compile(r'http|^[a-zA-Z]$|org')
    filtered=[i for i in words if not regex_clean.match(i)]
    filtered[:] = (value.lower() for value in filtered if len(value)!=0)
    return filtered
def calc_word_count(in_path, expected_path):
    word_counts = {'paranormal':defaultdict(int), 'skeptic': defaultdict(int)} # dzienik zawierajacy slownik w ktorym s slowa i ile razy wystepuja
    with open(in_path,encoding='utf-8') as infile, open(expected_path,encoding='utf-8')  as  expectedfile:
        for line, exp in zip(infile, expectedfile):
            class_ = exp.rstrip('\n').replace(' ','')
            tokens = tokenize_string(line)
            for token in tokens:
                if class_ == 'P':
                    word_counts['paranormal'][token] += 1
                elif class_ == 'S':
                    word_counts['skeptic'][token]+=1
    print(word_counts['skeptic'])
    return word_counts
def calc_word_logprobs(word_counts):
    total_skeptic = sum(word_counts['skeptic'].values()) + len(word_counts['skeptic'].keys())
    total_paranormal = sum(word_counts['paranormal'].values())+ len(word_counts['paranormal'].keys())

    word_logprobs= {'paranormal': {}, 'skeptic': {}}
    for class_ in word_counts.keys(): # sceptic paranormal
        for token, value in word_counts[class_].items():
            if class_ == 'skeptic':
                word_prob = (value +1)/ total_skeptic
            else:
                word_prob = (value+1)/ total_paranormal

            word_logprobs[class_][token] = math.log(word_prob)
    
    print(word_logprobs)
    return word_logprobs
def predict(word_counts,word_logprobs,x_prob,y_prob,path_in,path_out):
    skeptic_prob=0
    paran_prob=0
    total_skeptic = sum(word_counts['skeptic'].values()) + len(word_counts['skeptic'].keys())
    total_paranormal = sum(word_counts['paranormal'].values())+ len(word_counts['paranormal'].keys())
    sk_log=1/total_skeptic
    pa_log=1/total_paranormal
    print(math.log(sk_log))
    with open(path_in,encoding='utf-8') as d, open(path_out,'w+',encoding='utf-8') as o:
        for line in d:
            word_tokens=tokenize_string(line)
            
            for class_ in word_logprobs.keys(): # sceptic paranormal
                if class_ == 'skeptic':
                    for token in word_tokens:
                        if token in word_logprobs[class_].keys():
                                skeptic_prob+= word_logprobs[class_][token]
                        else:
                                skeptic_prob+= math.log(sk_log)
                else:
                    for token in word_tokens:
                        if token in word_logprobs[class_].keys():
                                paran_prob+= word_logprobs[class_][token]
                        else:
                                paran_prob+= math.log(pa_log)
            skeptic_prob+=x_prob
            paran_prob+=y_prob
            #print(skeptic_prob)
            #print(paran_prob)
            if skeptic_prob>paran_prob:
                    o.write('0\n')
            else:
                    o.write('1\n')
            skeptic_prob=0
            paran_prob=0

def main():
    
    paranomal_class_lgprob, skeptic_class_logprob = calc_class_logprob("D:/IS2020/train/expected.tsv")
    wordcounts =calc_word_count("D:/IS2020/train/in.tsv","D:/IS2020/train/expected.tsv")
    word_logprobs = calc_word_logprobs(wordcounts)
    predict(wordcounts,word_logprobs, skeptic_class_logprob,paranomal_class_lgprob,"D:/IS2020/dev-0/in.tsv","D:/IS2020/dev-0/out.tsv")
     # w predict.py bierzemy ten wzor argmax P(w) iloczynP(w|c)

main()
